const stylistic = require('@stylistic/eslint-plugin')

const customized = stylistic.configs.customize({
  indent: 2,
  semi: false,
  jsx: true,
  commaDangle: 'always-multiline',
  quoteProps: 'as-needed',
  quotes: 'single',
})

module.exports = {
  extends: 'next/core-web-vitals',
  plugins: [
    '@stylistic',
  ],
  overrides: [
    {
      files: [
        '**/*.tsx',
        '*.js',
      ],
      rules: {
        ...customized.rules,
      },
    },
  ],
}
