import { CardItem, MiniCardInfo } from './CardItem'

export
function InfoCards({ cards }: { cards: MiniCardInfo[] }) {
  return (
    <div className="mb-32 grid text-center lg:mb-0 lg:w-full lg:max-w-5xl lg:grid-cols-4 lg:text-left">
      {cards.map(info => <CardItem key={info.title} {...info} />)}
    </div>
  )
}
