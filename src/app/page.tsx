import { InfoCards, NextJsLogo, OneLineReadme, VercelLogo } from '@/components/Home'

const cards = [
  {
    title: 'Docs',
    link: 'https://nextjs.org/docs',
    copy: 'Find in-depth information about Next.js features and API.',
  },
  {
    title: 'Learn',
    link: 'https://nextjs.org/learn',
    copy: 'Learn about Next.js in an interactive course with quizzes!',
  },
  {
    title: 'Templates',
    link: 'https://vercel.com/templates?framework=next.js&',
    copy: 'Explore starter templates for Next.js.',
  },
  {
    title: 'Deploy',
    link: 'https://vercel.com/new',
    copy: 'Instantly deploy your Next.js site to a shareable URL with Vercel.',
  },
]

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <div className="z-10 w-full max-w-5xl items-center justify-between font-mono text-sm lg:flex">
        <OneLineReadme />
        <VercelLogo />
      </div>
      <NextJsLogo />
      <InfoCards cards={cards} />
    </main>
  )
}
